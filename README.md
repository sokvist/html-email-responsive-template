# HC Responsive Email Template

A table-based (but responsive) email template. Has Campaign Monitor tags in place. Based on the following work:

* ["Responsive Email Template"](https://github.com/philwareham/responsive-email)
* ["Campaign Monitor: Responsive Email Design"](http://www.campaignmonitor.com/guides/mobile/)
* ["From Monitor To Mobile: Optimizing Email Newsletters With CSS"](http://coding.smashingmagazine.com/2011/08/18/from-monitor-to-mobile-optimizing-email-newsletters-with-css/)
* ["Email Boilerplate"](https://github.com/seanpowell/Email-Boilerplate)
* ["MailChimp Email Blueprints"](https://github.com/mailchimp/Email-Blueprints)


## Tips for coders
* Be sure to stick to margins because paragraph padding is not supported by Outlook 2007/2010.
* Remember: Hotmail does not support *"margin"* nor the *"margin-top"* properties.
* Stick to *"margin-bottom"*, *"margin-left"*, *"margin-right"* in order to control spacing.
* It also wise to set the inline top-margin to "0" for consistancy in Gmail for every inline instance of a paragraph tag.
* Images hidden using `display:none;` do still load, even though they are not displayed.
* Make sure you link all your images using full URL paths, and upload them to a suitable webspace, or upload/link via your email transmission service.
* When using links within your email, make sure to include the `target="_blank"` attribute in your anchor elements to ensure they open a new browser window or tab when emails are viewed in browser based email clients.
* Avoid using `img align=""` when using an image as a link, it causes a strange behaviour in Outlook.

## Tips for designers and copywriters
* Recommended max width is 650px, [see post in Campaign Monitor](http://www.campaignmonitor.com/blog/post/3481/how-wide-are-html-email-designs-today/).
* Outlook, Yahoo Mail, Gmail and Windows Live Mail don’t support custom fonts.
* Outlook does not support background images (important when you don’t use a solid colour for the background)
* Lotus Notes does not support the display of PNG images.
* Avoid using a URL as the display text for a link to [avoid being identified as a potential phishing scammer](http://help.campaignmonitor.com/topic.aspx?t=135).
* How to [avoid getting your campaigns accidentally junked](http://kb.mailchimp.com/article/how-spam-filters-think/)
